<?php

require 'class.api.php';

$api = new API;

switch ($_GET['action']) {
    case 'getBooking':
        header('Content-Type: application/json');
        echo $api->getBooking($_GET['id']);
        break;
    case 'removeBooking':
        header('Content-Type: application/json');
        echo $api->removeBooking($_GET['id']);
        break;
    case 'editBooking':
        header('Content-Type: application/json');
        echo $api->editBooking($_GET['id']);
        break;
    case 'getMonthSchedule':
        header('Content-Type: application/json');
        echo $api->getMonthSchedule($_GET['year'], $_GET['month']);
        break;
    case 'ics':
        header('Content-Type: text/calendar; charset=utf-8');
        header('Content-Disposition: attachment; filename=anabasis_timeframes.ics');
        echo $api->ics($_GET['publicOnly']);
        break;
}


?>