<?php

class API {

    protected $db = null;

    protected $db_file_path = '../db/db.sqlite';

    function __construct () {
        header('Access-Control-Allow-Origin: *');
        try {
            $this->db = new SQLite3($this->db_file_path);
        } catch (Exception $exception) {
            echo $exception->getMessage();
            exit;
        }
    }

    public function getMonthSchedule ($year = 1970, $month = 1) {

        $date = $year . '-' . $month . '-01';
        $date_from = strtotime(date('Y-m-d', strtotime($date)));
        $date_to = strtotime(date('Y-m-t 23:59:59', strtotime($date)));

        $query = $this->db->prepare('
            SELECT *
            FROM `schedule`
            WHERE `time_to` >= :date_from
            AND `time_from` <= :date_to
        ');

        $query->bindParam(':date_from', $date_from);
        $query->bindParam(':date_to', $date_to);

        $result = $query->execute();

        $items = [];
        while ($row = $result->fetchArray()) {
            $items[] = [
                'id' => $row['id'],
                'room' => $row['room'],
                'title' => $row['title'],
                'text' => $row['text'],
                'time_from' => $row['time_from'],
                'time_to' => $row['time_to'],
                'noise' => $row['noise'],
                'silence' => $row['silence'],
                'public' => $row['public'],
            ];
        }
        
        return json_encode($items);

    }

    public function getBooking ($id = 0) {

        $query = $this->db->prepare("
            SELECT *
            FROM `schedule`
            WHERE `id` = :id
        ");

        $query->bindParam(':id', $id);
        
        $result = $query->execute();

        $item = new stdClass();
        while ($row = $result->fetchArray()) {
            $item = [
                'id' => $row['id'],
                'room' => $row['room'],
                'title' => $row['title'],
                'text' => $row['text'],
                'time_from' => $row['time_from'],
                'time_to' => $row['time_to'],
                'noise' => $row['noise'],
                'silence' => $row['silence'],
                'public' => $row['public'],
            ];
        }

        return json_encode($item);

    }

    public function editBooking ($id = 0) {

        $post = json_decode(file_get_contents('php://input'), true);

        if ($id) {
            $query = $this->db->prepare('
                UPDATE `schedule`
                SET `title` = :title,
                    `text` = :text,
                    `time_from` = :time_from,
                    `time_to` = :time_to,
                    `room` = :room,
                    `noise` = :noise,
                    `silence` = :silence,
                    `public` = :public
                WHERE `id` = :id
            ');
            $query->bindParam(':id', $id);
        } else {
            $query = $this->db->prepare('               
                INSERT INTO `schedule`
                (`title`, `text`, `time_from`, `time_to`, `room`, `noise`, `silence`, `public`)
                VALUES
                (:title, :text, :time_from, :time_to, :room, :noise, :silence, :public)
            ');
        }

        $query->bindParam(':title', $post['title']);
        $query->bindParam(':text', $post['text']);
        $query->bindParam(':time_from', $post['time_from']);
        $query->bindParam(':time_to', $post['time_to']);
        $query->bindParam(':room', $post['room']);
        $query->bindParam(':noise', $post['noise']);
        $query->bindParam(':silence', $post['silence']);
        $query->bindParam(':public', $post['public']);
        
        $query->execute();

        return json_encode($post);

    }

    public function removeBooking ($id = 0) {

        $this->db->query("
            DELETE
            FROM `schedule`
            WHERE `id` = '$id'
        ");

        return true;

    }

    public function ics ($publicOnly) {

        $date_from = strtotime(date('Y-m-t', time()));
        $date_from = strtotime(date('Y-m-t', strtotime('-3 months', $date_from)));
        $date_to = strtotime(date('Y-m-t', strtotime('+3 months', $date_from)));

        $publicSqlWhere = (isset($publicOnly)) ? 'AND `public` = 1' : '';
        
        $query = $this->db->prepare('
            SELECT *
            FROM `schedule`
            WHERE `time_to` >= :date_from
            AND `time_from` <= :date_to
            '.$publicSqlWhere
        );

        $query->bindParam(':date_from', $date_from);
        $query->bindParam(':date_to', $date_to);

        $result = $query->execute();

        $output = "BEGIN:VCALENDAR\r\n";
        $output .= "VERSION:2.0\r\n";
        $output .= "PRODID:-//ANABASIS//EN\r\n";
        $output .= "CALSCALE:GREGORIAN\r\n";

        while ($row = $result->fetchArray()) {

            $description = '';
            if ($row['text']) {
                $description = str_replace("\n", "\\n", $row['text']);
                $description = str_replace(",", "\,", $description);
                $description = str_replace(":", "\:", $description);
                $description = str_replace(";", "\;", $description);
                $description = str_replace("\"", "'", $description);
                $description = "\r\n ". wordwrap($description, 74, "\r\n ", true);
            }

            $output .= "BEGIN:VEVENT\r\n";
            $output .= "UID:". $row['id'] ."\r\n";
            $output .= "DTSTAMP:". date('Ymd', $row['time_from']) ."T". date('His', $row['time_from']) ."Z\r\n";
            $output .= "DTSTART:". date('Ymd', $row['time_from']) ."\r\n";
            $output .= "DTEND:". date('Ymd', $row['time_to']) ."\r\n";
            $output .= "SUMMARY:". substr($row['title'], 0, 75)."\r\n";
            $output .= "DESCRIPTION:". $description ."\r\n";
            $output .= "END:VEVENT\r\n";
        }

        $output .= "END:VCALENDAR\r\n";

        return $output;

    }

}

?>