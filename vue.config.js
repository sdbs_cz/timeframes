module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/timeframes/' : '/',
  devServer: {
    proxy: {
      '^/api': {
        target: 'https://anabasis.sdbs.cz/timeframes/',
        changeOrigin: true
      },
    }
  },
  pages: {
    index: {
      entry: 'src/main.js',
      template: 'public/index.html',
      filename: 'index.html',
      title: 'Anabasis timeframes',
      chunks: ['chunk-vendors', 'chunk-common', 'index']
    }
  }
}
